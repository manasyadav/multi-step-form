document.addEventListener("DOMContentLoaded", () => {
  let toggle = document.querySelector("#check");
  let arp = document.querySelector("#arcade-price");
  let ap = document.querySelector("#advance-price");
  let pp = document.querySelector("#pro-price");
  let freebies = document.querySelectorAll(".freebie");
  let onlineAddOn = document.querySelector("#online-addon");
  let storageAddOn = document.querySelector("#storage-addon");
  let profileAddOn = document.querySelector("#profile-addon");

  toggle.addEventListener("change", () => {
    if (toggle.checked) {
      for (let freebie of freebies) {
        freebie.style.display = "block";
      }
      arp.innerHTML = "&dollar;90/yr";
      ap.innerHTML = "&dollar;120/yr";
      pp.innerHTML = "&dollar;150/yr";
      onlineAddOn.innerHTML = "&dollar;10/yr";
      storageAddOn.innerHTML = "&dollar;20/yr";
      profileAddOn.innerHTML = "&dollar;20/yr";
    } else {
      for (let freebie of freebies) {
        freebie.style.display = "none";
      }
      arp.innerHTML = "&dollar;9/mo";
      ap.innerHTML = "&dollar;12/mo";
      pp.innerHTML = "&dollar;15/mo";
      onlineAddOn.innerHTML = "&dollar;1/mo";
      storageAddOn.innerHTML = "&dollar;2/mo";
      profileAddOn.innerHTML = "&dollar;2/mo";
    }
  });

  function signUpPage() {
    let firstPage = document.querySelector(".first");
    firstPage.style.display = "block";

    const tasks = document.querySelectorAll(".task");
    tasks[0].style.display = "block";

    let submit = document.querySelector("#info-submit");
    submit.addEventListener("click", (e) => {
      e.preventDefault();
      if (
        validateInput("name") &&
        validateInput("email") &&
        validateInput("phone")
      ) {
        firstPage.style.display = "none";
        tasks[0].style.display = "none";

        planPage();
      }
    });
  }

  function planPage() {
    let secondPage = document.querySelector(".second");
    secondPage.style.display = "flex";

    const tasks = document.querySelectorAll(".task");
    tasks[1].style.display = "flex";

    let next = document.querySelector("#next");
    next.addEventListener("click", (e) => {
      e.preventDefault();
      let arcade = document.querySelector("#arcade");
      let advance = document.querySelector("#advance");
      let pro = document.querySelector("#pro");
      if (arcade.checked || advance.checked || pro.checked) {
        secondPage.style.display = "none";
        tasks[1].style.display = "none";
        addOnPage();
      }
    });

    let back1 = document.getElementById("back1");
    back1.addEventListener("click", (e) => {
      e.preventDefault();
      secondPage.style.display = "none";
      tasks[1].style.display = "none";

      signUpPage();
    });
  }

  function addOnPage() {
    let thirdPage = document.querySelector(".third");
    thirdPage.style.display = "flex";

    const tasks = document.querySelectorAll(".task");
    tasks[2].style.display = "flex";

    let next = document.querySelector(".third #next");
    next.addEventListener("click", (e) => {
      e.preventDefault();
      thirdPage.style.display = "none";
      tasks[2].style.display = "none";

      summaryPage();
    });

    let back2 = document.getElementById("back2");
    back2.addEventListener("click", (e) => {
      e.preventDefault();
      thirdPage.style.display = "none";
      tasks[2].style.display = "none";
      planPage();
    });
  }

  function summaryPage() {

    let priceDisplay = document.querySelector('.online-service-and-storage');
    
    let confirm  = document.querySelector('#confirm');
    let confirmationPage = document.querySelector('#confirmation');

    let fourthPage = document.querySelector(".fourth");
    fourthPage.style.display = "flex";

    const tasks = document.querySelectorAll(".task");
    tasks[3].style.display = "flex";

    let change = document.querySelector("#change");
    change.addEventListener("click", (e) => {
      e.preventDefault();
      tasks[3].style.display = "none";
      fourthPage.style.display = "none";

      priceDisplay.style.

      planPage();
    });

    let mode = document.querySelector(".mode-name");
    let modePrice = document.querySelector(".mode-price");

    let arcade = document.querySelector("#arcade");
    let advance = document.querySelector("#advance");
    let pro = document.querySelector("#pro");

    let onlineService = document.querySelector("#online-service");
    let storage = document.querySelector("#storage");
    let profile = document.querySelector("#profile");

    let onlinePrice = document.querySelector(".online-service-price");
    let storagePrice = document.querySelector(".larger-storage-price");
    let profilePrice = document.querySelector(".customizable-profile-price");

    let onlineBox = document.querySelector(".online-service");
    let storageBox = document.querySelector(".larger-storage");
    let profileBox = document.querySelector(".customizable-profile");

    let total = 0;
    let totalPrice = document.querySelector(".total-bill-price");
    let totalTime = document.querySelector(".total-bill-name");

    if (toggle.checked) {
      if (arcade.checked) {
        mode.innerText = "Arcade(yearly)";
        modePrice.innerText = "$90/yr";
        total += 90;
      } else if (advance.checked) {
        mode.innerText = "Advance(yearly)";
        modePrice.innerText = "$120/yr";
        total += 120;
      } else {
        mode.innerText = "Pro(yearly)";
        modePrice.innerText = "$150/yr";
        total += 150;
      }

      if (onlineService.checked) {
        onlineBox.style.display = "flex";
        onlinePrice.innerText = "$10/yr";
        total += 10;
      }
      if (storage.checked) {
        storageBox.style.display = "flex";
        storagePrice.innerText = "$20/yr";
        total += 20;
      }
      if (profile.checked) {
        profileBox.style.display = "flex";
        profilePrice.innerText = "$20/yr";
        total += 20;
      }

      totalTime.innerHTML = "Total (per year)";
    } else {
      if (arcade.checked) {
        mode.innerText = "Arcade(monthly)";
        modePrice.innerText = "$9/mo";
        total += 9;
      } else if (advance.checked) {
        mode.innerText = "Advance(monthly)";
        modePrice.innerText = "$12/mo";
        total += 12;
      } else {
        mode.innerText = "Pro(monthly)";
        modePrice.innerText = "$15/mo";
        total += 15;
      }

      if (onlineService.checked) {
        onlineBox.style.display = "flex";
        onlinePrice.innerText = "$1/mo";
        total += 1;
      }
      if (storage.checked) {
        storageBox.style.display = "flex";
        storagePrice.innerText = "$2/mo";
        total += 2;
      }
      if (profile.checked) {
        profileBox.style.display = "flex";
        profilePrice.innerText = "$2/mo";
        total += 2;
      }

      totalTime.innerHTML = "Total (per year)";
    }

    totalPrice.innerHTML = `$${total}/yr`;

    let back3 = document.getElementById("back3");
    back3.addEventListener("click", (e) => {
      e.preventDefault();
      fourthPage.style.display = "none";
      tasks[3].style.display = "none";

      addOnPage();
    });

    confirm.addEventListener('click', (e) => {
      e.preventDefault();
      fourthPage.style.display = 'none';
      tasks[3].style.display = "none";
      confirmationPage.style.display = "flex";
      let container = document.querySelector(".container");
      container.style.display = "block";
      
    });

  }

  

  function validateInput(inputId) {
    const input = document.getElementById(inputId);
    const errorMessage = document.getElementById(`${inputId}-error`);

    if (!input.value.trim()) {
      errorMessage.textContent = "This field is required";
      errorMessage.style.display = "block";
      errorMessage.style.color = "hsl(354, 84%, 57%)";
      return false;
    } else {
      errorMessage.textContent = "";
      errorMessage.style.display = "none";
      return true;
    }
  }

  signUpPage();
});

function checkMail(){
  console.log("afasfd");
  let err = document.querySelector("#email-error");
  err.innerText = "Invalid email"; 
  err.style.display = "block";
}
